
from importlib.resources import path
from posixpath import dirname
import pytest
from botocore.stub import Stubber
import botocore.session
import os
import sys

sys.path.insert(0, os.path.abspath(
    os.path.join(os.path.dirname(__file__), "../")))
from helpers.s3client import S3Client

s3_response = {
        "ResponseMetadata": {
            "RequestId": "5994D680BF127CE3",
            "HTTPStatusCode": 200,
            "RetryAttempts": 1,
        },
        "ETag": '"6299528715bad0e3510d1e4c4952ee7e"',
    }

@pytest.mark.parametrize("s3_response", [s3_response])
def test_s3_putobject(s3_response):
    s3 = botocore.session.get_session().create_client('s3', 'us-east-1')
    stubber = Stubber(s3)

    expected_param = {'Body': 'test.txt', 'Bucket': 'klarnacsvnew', 'Key': 'test.txt'}
    stubber.add_response('put_object', s3_response, expected_param)
    stubber.activate()

    service_response = S3Client.s3_put_object(
        s3, "klarnacsvnew", "test.txt", "test.txt")

    assert service_response == s3_response
