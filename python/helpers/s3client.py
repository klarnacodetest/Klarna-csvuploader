from asyncio.log import logger
from urllib import response
from botocore.exceptions import ClientError
import boto3


class S3Client:

    def s3_client(aws_region):

        try:
            s3client = boto3.client('s3',
                                    region_name=aws_region)

            return s3client

        except ClientError as e:
            logger.info(e)

    def s3_put_object(client, bucket, file, key):

        try:

            response = client.put_object(
                Body=file,
                Bucket=bucket,
                Key=key,
            )

            return response

        except Exception as e:
            logger.info(e)
