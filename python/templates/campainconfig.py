class ConfigTemplate:
    @staticmethod
    def config_template(config_path, campain_name, s3_bucket):
        config_template = f"""
config_path: {config_path}
campain_name: {campain_name}
s3_bucket: {s3_bucket}
"""

        return config_template
