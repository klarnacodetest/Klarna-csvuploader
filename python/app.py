import json
import boto3
import base64

from templates.campainconfig import ConfigTemplate
from datetime import datetime
from asyncio.log import logger
from helpers.s3client import S3Client


def lambda_handler(event, context):

    s3_client = S3Client.s3_client("us-east-1")
    s3_bucket = "klarnacsvnew"

    file_content = event["body"]
    content_decoded = base64.b64decode(file_content)
    campain_id = event["headers"]["campain_id"]

    date = datetime.today().strftime('%Y-%m-%d')

    destination_data_path = "data/"+date+"/campaindata.csv"
    config_path = "configs/campainconfig.json"

    config_data = {
        "s3_bucket":  s3_bucket,
        "config_path": destination_data_path,
        "campain_id": campain_id
    }

    s3_upload_data = S3Client.s3_put_object(
        s3_client, s3_bucket, content_decoded, destination_data_path)
    s3_upload_config = S3Client.s3_put_object(
        s3_client, s3_bucket, json.dumps(config_data), config_path)

    return {
        'data_upload': s3_upload_data,
        'config_upload': s3_upload_config
    }
