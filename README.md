# Klarna Csvuploader

this is use to upload csv files to aws s3 bucket

## Deploy Klarna-csvuploader

* Create an ECR klarna-csvuploader.
* Clone the repo
    ```
    Git clone https://gitlab.com/klarnacodetest/Klarna-csvuploader.git
    ```
* Go inside the project and build the docker image.
    ```
    cd Klarna-csvuploader
    docker build -t  klarna-csvuploader .
    docker tag klarna-csvuploader:latest <-aws account -d->.dkr.ecr.us-east-1.amazonaws.com/klarna-csvuploader:latest
    docker push <-aws account -d->..dkr.ecr.us-east-1.amazonaws.com/uploadcsv:latest
    ```
* Create S3 bucket to store csv files.
    - Go to aws console & navigate to s3 then create a s3 bucket with name klarna-csvuploader.
* Create the lambda function klarna-csvuploader
    - Create IAM for lambda function that should have S3:putobject access. You can find the IAM policy from permission dir in repo.
    - Navigate to the lambda section in aws console.
    - Click the button create function in the top right corner.
    - Now chose the container image option provide the name for lambda function in this case name is klarna-csvuploader
    - Choose the upload docker image in the above step.
    - Choose the IAM role which was created.
    - Then create the lambda function.
